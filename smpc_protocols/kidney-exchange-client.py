#!/usr/bin/python3
import random
import sys

import sympy as sympy

sys.path.append('.')

from client import *
from domains import *

client_id = int(sys.argv[1])
n_computing_peers = int(sys.argv[2])
n_input_peers = int(sys.argv[3])
kep_rnd_flag = int(sys.argv[4])
altruists_flag = int(sys.argv[5])
finish = int(sys.argv[6])


client = Client(['localhost'] * n_computing_peers, 14000, client_id)

type = client.specification.get_int(4)

if type == ord('p'):
    domain = Fp(client.specification.get_bigint())
else:
    raise Exception('invalid type')

for socket in client.sockets:
    os = octetStream()
    os.store(finish)
    os.Send(socket)

input_data = []
with open("ExternalIO/Inputs/input_"+str(client_id)+".txt") as f:
    for l in f:
        input_data.append(l.split(" "))

# compatibility check input
donor_blood = []
patient_blood = []
donor_antigens_a = []
donor_antigens_b = []
donor_antigens_c = []
donor_antigens_dr = []
donor_antigens_dq = []
donor_antigens_dp = []
patient_antibodies_a = []
patient_antibodies_b = []
patient_antibodies_c = []
patient_antibodies_dr = []
patient_antibodies_dq = []
patient_antibodies_dp = []
# altruists input
altruist_bit = []
# prioritization input
prescores = []
patient_antigens_a = []
patient_antigens_b = []
patient_antigens_dr = []
patient_bloodtype = []
donor_bloodtype = []
patient_age = []
donor_age = []
patient_region = []
donor_region = []

for i in range(len(input_data)):
    for j in range(len(input_data[i])):
        if i == 0:
            donor_blood.append(domain(int(input_data[i][j].rstrip())))
        elif i == 1:
            donor_antigens_a.append(domain(int(input_data[i][j].rstrip())))
        elif i == 2:
            donor_antigens_b.append(domain(int(input_data[i][j].rstrip())))
        elif i == 3:
            donor_antigens_c.append(domain(int(input_data[i][j].rstrip())))
        elif i == 4:
            donor_antigens_dr.append(domain(int(input_data[i][j].rstrip())))
        elif i == 5:
            donor_antigens_dq.append(domain(int(input_data[i][j].rstrip())))
        elif i == 6:
            donor_antigens_dp.append(domain(int(input_data[i][j].rstrip())))
        elif i == 7:
            patient_blood.append(domain(int(input_data[i][j].rstrip())))
        elif i == 8:
            patient_antibodies_a.append(domain(int(input_data[i][j].rstrip())))
        elif i == 9:
            patient_antibodies_b.append(domain(int(input_data[i][j].rstrip())))
        elif i == 10:
            patient_antibodies_c.append(domain(int(input_data[i][j].rstrip())))
        elif i == 11:
            patient_antibodies_dr.append(domain(int(input_data[i][j].rstrip())))
        elif i == 12:
            patient_antibodies_dq.append(domain(int(input_data[i][j].rstrip())))
        elif i == 13:
            patient_antibodies_dp.append(domain(int(input_data[i][j].rstrip())))
        elif i == 14:
            altruist_bit.append(domain((int(input_data[i][j].rstrip()))))
        elif i == 15:
            prescores.append(domain(int(input_data[i][j].rstrip())))
        elif i == 16:
            patient_antigens_a.append(domain(int(input_data[i][j].rstrip())))
        elif i == 17:
            patient_antigens_b.append(domain(int(input_data[i][j].rstrip())))
        elif i == 18:
            patient_antigens_dr.append(domain(int(input_data[i][j].rstrip())))
        elif i == 19:
            patient_bloodtype.append(domain(int(input_data[i][j].rstrip())))
        elif i == 20:
            donor_bloodtype.append(domain(int(input_data[i][j].rstrip())))
        elif i == 21:
            patient_age.append(domain(int(input_data[i][j].rstrip())))
        elif i == 22:
            donor_age.append(domain(int(input_data[i][j].rstrip())))
        elif i == 23:
            patient_region.append(domain(int(input_data[i][j].rstrip())))
        elif i == 24:
            donor_region.append(domain(int(input_data[i][j].rstrip())))

# only relevant for protocol KEP_Rnd
party_primes = []
prime_constellations = []
if kep_rnd_flag == 1:
    # Generates the primes
    # Compute range for this client's primes
    min_prime = sympy.prime(pow(n_input_peers, 2) * client_id + 1)
    max_prime = sympy.prime(pow(n_input_peers, 2) * (client_id + 1))

    prime_constellations = list(sympy.primerange(min_prime, max_prime + 1))
    random.shuffle(prime_constellations)

    for prime in prime_constellations:
        party_primes.append(domain(prime))

# send input for compatibility check
client.send_private_inputs(donor_blood)
client.send_private_inputs(patient_blood)
client.send_private_inputs(donor_antigens_a)
client.send_private_inputs(patient_antibodies_a)
client.send_private_inputs(donor_antigens_b)
client.send_private_inputs(patient_antibodies_b)
client.send_private_inputs(donor_antigens_c)
client.send_private_inputs(patient_antibodies_c)
client.send_private_inputs(donor_antigens_dr)
client.send_private_inputs(patient_antibodies_dr)
client.send_private_inputs(donor_antigens_dq)
client.send_private_inputs(patient_antibodies_dq)
client.send_private_inputs(donor_antigens_dp)
client.send_private_inputs(patient_antibodies_dp)

# send primes for protocol KEP_Rnd
if kep_rnd_flag == 1:
    client.send_private_inputs(party_primes[:n_input_peers*n_input_peers])

# send altruist bits
if altruists_flag == 1:
    client.send_private_inputs(altruist_bit)

# send input for prioritization
client.send_private_inputs(prescores)
client.send_private_inputs(patient_antigens_a)
client.send_private_inputs(patient_antigens_b)
client.send_private_inputs(patient_antigens_dr)
client.send_private_inputs(donor_antigens_a)
client.send_private_inputs(donor_antigens_b)
client.send_private_inputs(donor_antigens_dr)
client.send_private_inputs(patient_bloodtype)
client.send_private_inputs(donor_bloodtype)
client.send_private_inputs(patient_age)
client.send_private_inputs(donor_age)
client.send_private_inputs(patient_region)
client.send_private_inputs(donor_region)

if kep_rnd_flag == 0:
    donor = client.receive_outputs(domain, 1)[0].v % 2 ** 64
    print("Client"+str(client_id+1)+": The donor for your patient is: "+str(donor))

    patient = client.receive_outputs(domain, 1)[0].v % 2 ** 64
    print("Client"+str(client_id+1)+": The recipient for your donor is: "+str(patient))
elif kep_rnd_flag == 1:
    matching_prime = client.receive_outputs(domain, 1)[0].v

    donor = None
    recipient = None
    found_partners = False

    if matching_prime == 0:
        found_partners = True

    for i in range(n_input_peers):
        if not found_partners:
            for j in range(n_input_peers):
                if matching_prime % prime_constellations[i * n_input_peers + j] == 0:
                    donor = i+1
                    recipient = j+1
                    found_partners = True
                    break

    if donor == client_id+1 or donor is None:
        output_string_donor = "The donor for your patient is: None"
    else:
        output_string_donor = "The donor for your patient is: " + str(donor)

    if recipient == client_id+1 or recipient is None:
        output_string_patient = "The recipient for your donor is: None"
    else:
        output_string_patient = "The recipient for your donor is: " + str(recipient)

    # donor = client.receive_outputs(domain, 1)[0].v % 2 ** 64
    print("Client" + str(client_id+1) + ":" + output_string_donor)

    # patient = client.receive_outputs(domain, 1)[0].v % 2 ** 64
    print("Client" + str(client_id+1) + ":" + output_string_patient)