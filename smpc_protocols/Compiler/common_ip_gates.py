from Compiler.types import sint, Array, Matrix
from Compiler.library import for_range
from Compiler.util import if_else


def get_edge_mapping(subsets):
    """
    Compute the mapping between each subset and the corresponding cycles. Note that for subsets of size 2 there is
    only one possible cycle and for subsets of size 3 there are two possible cycles. For coding simplicity we store
    two values for each subset where the second value for a subset of size 2 just contains an empty list.
    """
    edge_mapping = []

    for s in subsets:
        edges1 = []
        edges2 = []
        if len(s) == 2:
            edges1.append([s[0], s[1]])
            edges1.append([s[1], s[0]])
            edge_mapping.append(edges1)
            edge_mapping.append([])
        if len(s) == 3:
            edges1.append([s[0], s[1]])
            edges1.append([s[1], s[2]])
            edges1.append([s[2], s[0]])

            edges2.append([s[0], s[2]])
            edges2.append([s[2], s[1]])
            edges2.append([s[1], s[0]])

            edge_mapping.append(edges1)
            edge_mapping.append(edges2)

    return edge_mapping


def ip_decryption_phase(solution_matrix, num_nodes):
    """
    Perform the mapping from the solution matrix to each party's exchange partners.
    """
    donors = Array(num_nodes, sint)
    recipients = Array(num_nodes, sint)

    donor = Array(1, sint)
    recipient = Array(1, sint)

    @for_range(num_nodes)
    def _(i):
        donor[0] = sint(0)
        recipient[0] = sint(0)

        @for_range(num_nodes)
        def _(j):
            donor[0] = donor[0] + (j + 1) * solution_matrix[j][i]
            recipient[0] = recipient[0] + (j + 1) * solution_matrix[i][j]

        donors[i] = donor[0]
        recipients[i] = recipient[0]

    return donors, recipients


def ip_resolution_phase(x_prime, mapping, edge_mapping, num_nodes, s_length):
    """
    Transform the solution vector x_prime (indicating which subsets are chosen) into a solution matrix y where each
    entry encodes which input peer donates a kidney to which other input peer.
    """
    solution_matrix = Matrix(num_nodes, num_nodes, sint)
    solution_matrix.assign_all(0)

    y = Matrix(s_length, 2, sint)

    # y[i][j] indicates whether cycle C[i][j] with vertex set V(C[i][j]) = S[i] is among the selected
    # exchange cycles. This is the case if and only if x_prime = 1 (i.e., set S[i] is selected) and M[i][j] = 1
    # (i.e., c(S[i]) = C[i][j]).
    @for_range(s_length)
    def _(i):
        @for_range(2)
        def _(j):
            y[i][j] = x_prime[i] * mapping[i][j]

    # Transform the selected exchange cycles (with y[i][j] = 1) into the edges that make up the exchange.
    # For each edge e, we sum up those variables y[i][j] corresponding to cycles C[i][j] with e \in E(C[i][j]). This
    # results in a solution matrix where entry solution_matrix[i][j] = 1 indicates that input peer i donates a kidney
    # to input peer j. We need to use standard python loops as the edge mapping is not an MP-SPDZ datastructure.
    for pi in range(s_length):
        for pj in range(2):
            for pe in edge_mapping[2 * pi + pj]:
                solution_matrix[pe[0] - 1][pe[1] - 1] = solution_matrix[pe[0] - 1][pe[1] - 1] + y[pi][pj]

    return solution_matrix


def min_sel(r_vec):
    """
    Take a vector r_vec and return a new vector r_vec_prime where only the first positive entry of r_vec is kept.
    """
    prev_sum = Array(1, sint)
    r_vec_prime = Array(r_vec.length, sint)

    prev_sum[0] = sint(0)

    @for_range(r_vec_prime.length)
    def _(i):
        r_vec_prime[i] = if_else(r_vec[i] > prev_sum[0], sint(1), sint(0))
        prev_sum[0] = prev_sum[0] + r_vec[i]

    return r_vec_prime