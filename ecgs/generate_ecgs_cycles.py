import pickle
import os
import shutil
import sys

TEMP_DIR = "temp"
TEMP_FILE_PREFIX_2 = TEMP_DIR + "/temp_c2_p"
TEMP_FILE_PREFIX_3 = TEMP_DIR + "/temp_c3_p"
FINAL_FILE_PREFIX = "ecgs_c3_"


# Returns list of all possible 2-cycles for the specified list of clients
def get_2_cycles(clients):
    cycles = []
    for i in clients:
        for j in range(i + 1, clients[-1] + 1):
            cycles.append((i, j))
    return cycles


# Returns list of all possible 3-cycles for the specified list of clients
def get_3_cycles(clients):
    cycles = []
    for i in clients:
        for j in range(i + 1, clients[-1] + 1):
            for k in range(j + 1, clients[-1] + 1):
                cycles.append((i, j, k))
                cycles.append((i, k, j))
    return cycles


def expand_graph_by_2_cycle(graph, possible_cycles):
    result = []
    graph = list(graph)
    for potential_cycle in possible_cycles:
        if potential_cycle[0] not in graph \
                and potential_cycle[1] not in graph \
                and potential_cycle[0] > graph[-2]:
            new_graph = graph.copy()
            new_graph += [potential_cycle[0], potential_cycle[1]]
            result.append(new_graph)
    return result


def expand_graph_by_3_cycle(graph, possible_cycles, is_first_three_cycle):
    result = []
    graph = list(graph)
    for potential_cycle in possible_cycles:
        # parties do not appear in the graph yet
        if potential_cycle[0] not in graph \
                and potential_cycle[1] not in graph \
                and potential_cycle[2] not in graph:
            # there exists no cycle in the graph
            # where the first party of that cycle has a lower id than the first party of the (current) potential cycle
            if is_first_three_cycle or not potential_cycle[0] <= graph[-3]:
                new_graph = graph.copy()
                new_graph += [potential_cycle[0], potential_cycle[1], potential_cycle[2]]
                result.append(new_graph)
    return result


def reformat_graph(graph, num_clients, num_2_cycles, num_3_cycles):
    result = []
    for i in range(0, 2 * num_2_cycles, 2):
        result += [graph[i], graph[i + 1], 0]
    for i in range(2 * num_2_cycles, 2 * num_2_cycles + 3 * num_3_cycles, 3):
        result += [graph[i], graph[i + 1], graph[i+2], 0]
    for i in range(1, num_clients + 1):
        if i not in result:
            result += [i, 0]
    result += [0] * (2 * num_clients - len(result))
    return result


def write_reformatted_graphs_to_file(graphs, file):
    with open(file, "a") as outfile:
        for graph in graphs:
            outfile.write(" ".join(map(str, graph)) + "\n")


def write_graphs_to_file(graphs, file):
    with open(file, "wb") as outfile:
        pickle.dump(graphs, outfile)
    write_reformatted_graphs_to_file(graphs, file + "_readable")


def read_graphs_from_file(file):
    with open(file, "rb") as infile:
        return pickle.load(infile)


def extend_by_three_cycles(current_2_cycle_num, num_clients, three_cycles):
    shutil.copyfile(TEMP_FILE_PREFIX_2 + str(current_2_cycle_num), TEMP_FILE_PREFIX_3 + str(current_2_cycle_num) + "_0")
    shutil.copyfile(TEMP_FILE_PREFIX_2 + str(current_2_cycle_num) + "_readable",
                    TEMP_FILE_PREFIX_3 + str(current_2_cycle_num) + "_0" + "_readable")
    for current_3_cycle_num in range(1, int((num_clients - 2 * current_2_cycle_num) / 3) + 1):
        previous_graphs = read_graphs_from_file(
            TEMP_FILE_PREFIX_3 + str(current_2_cycle_num) + "_" + str(current_3_cycle_num - 1))

        if len(previous_graphs) == 0:
            write_graphs_to_file(dict.fromkeys(three_cycles), TEMP_FILE_PREFIX_3 + str(current_2_cycle_num) + "_" + str(current_3_cycle_num))
        else:
            current_graphs = []
            for graph in previous_graphs:
                current_graphs += expand_graph_by_3_cycle(graph, three_cycles, current_3_cycle_num == 1)

            output = dict.fromkeys([tuple(x) for x in current_graphs])
            write_graphs_to_file(output, TEMP_FILE_PREFIX_3 + str(current_2_cycle_num) + "_" + str(current_3_cycle_num))


def generate_cycles(num_clients):
    print("Generating ecgs for "+str(num_clients) + "pairs. This may take some time.")
    # Folder for temporary files
    os.mkdir(TEMP_DIR)

    destination_file = "ecgs/"+FINAL_FILE_PREFIX

    clients = []
    for i in range(num_clients):
        clients.append(int(i + 1))

    two_cycles = get_2_cycles(clients)
    three_cycles = get_3_cycles(clients)

    # Graphs with 0 2-Cycles
    write_graphs_to_file([], TEMP_FILE_PREFIX_2 + "0")
    extend_by_three_cycles(0, num_clients, three_cycles)

    # Graphs with 1 2-Cycle
    print(f"Generating Cycle C2 1 / C3 0...")
    graphs_one_cycle = [(tuple(x)) for x in two_cycles]
    output = dict.fromkeys(graphs_one_cycle)
    write_graphs_to_file(output, TEMP_FILE_PREFIX_2 + "1")

    extend_by_three_cycles(1, num_clients, three_cycles)

    # Graphs with 2 Cycles to n/2 Cycles
    for current_2_cycle_num in range(2, int(num_clients / 2) + 1):
        previous_graphs = read_graphs_from_file(TEMP_FILE_PREFIX_2 + str(current_2_cycle_num - 1))

        current_graphs = []
        for graph in previous_graphs:
            current_graphs += expand_graph_by_2_cycle(graph, two_cycles)

        output = dict.fromkeys([tuple(x) for x in current_graphs])
        write_graphs_to_file(output, TEMP_FILE_PREFIX_2 + str(current_2_cycle_num))

        extend_by_three_cycles(current_2_cycle_num, num_clients, three_cycles)

    # Reformat to form as required by public input file
    with open(destination_file + str(num_clients), "w") as outfile:  # remove file contents and write 0th graph
        for i in range(1, num_clients + 1):
            outfile.write(f"{i} 0 ")
        outfile.write("\n")

    current_graphs = []

    for current_2_cycle_num in range(0, int(num_clients / 2) + 1):
        for current_3_cycle_num in range(0, int((num_clients - 2 * current_2_cycle_num) / 3) + 1):
            if current_2_cycle_num == current_3_cycle_num == 0:
                continue

            graphs = read_graphs_from_file(TEMP_FILE_PREFIX_3 + str(current_2_cycle_num) + "_" + str(current_3_cycle_num))
            for graph in graphs:
                current_graphs.append(reformat_graph(graph, num_clients, current_2_cycle_num, current_3_cycle_num))

    write_reformatted_graphs_to_file(current_graphs, destination_file + str(num_clients))

    # Cleans up temporary files
    shutil.rmtree(TEMP_DIR)


def main(input_peers=3):
    generate_cycles(input_peers)


if __name__ == "__main__":
    os.chdir(sys.path[0])
    if len(sys.argv) > 1:
        main(int(sys.argv[1]))
    else:
        main()
