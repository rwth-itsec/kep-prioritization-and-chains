import sys
from itertools import combinations
import itertools
import os
import numpy as np


def to_cycles(permutation, n):
    res = []
    left = []
    for i in range(n):
        left.append(i + 1)

    while len(left) > 0:
        current_cycle = []
        i = left[0]
        while i in left:
            left.remove(i)
            current_cycle.append(i)
            if permutation[i - 1] == i:
                break
            i = permutation[i - 1]
        res.append(tuple(current_cycle))
    return res


def reformat(permutations):
    result = []
    for perm in permutations:
        if perm is not None:
            result.append([list(x) for x in perm])
    return result


def altruists_to_front(cycle, altruists, max_chain_len):
    first_altruist_pos = -1
    for i in range(len(cycle)):
        if cycle[i] in altruists:
            # if multiple altruists are found, returns empty cycle
            if first_altruist_pos == -1:
                first_altruist_pos = i
            else:
                return []

    # if no altruist in current cycle
    if first_altruist_pos == -1:
        # delete cycle if length > 3
        if len(cycle) > 3:
            return []
        else:
            return cycle

    # filter long chains
    if len(cycle) > max_chain_len:
        return []

    new_cycle = [0] * len(cycle)
    for i in range(len(cycle)):
        new_cycle[i] = cycle[(first_altruist_pos + i) % len(cycle)]

    new_cycle_minus = new_cycle.copy()
    if len(new_cycle) > 1:
        new_cycle_minus[0] *= -1
    return tuple(new_cycle_minus)


# filters long cycles except those which start with altruists
# also filters cycles/chains with 2 or more altruists
# chains will be marked by multiplying the ids with -1

# in chains: altruist in front
# after chains: ids of altruists listed
def filter_long_cycles_keep_chains(permutations, finished_perms, altruists, max_chain_len):
    result = []
    result_altruists = []
    result_whatever_role = []
    i = 0
    for perm in permutations:
        i = i + 1
        # print(str(i) + " / " + str(len(permutations)))

        delete = False
        whatever_role = []
        for cyc_num, cycle in enumerate(perm):

            # if cycle contains one altruist, the cycle is rearranged so that the altruist is in front
            # if the cycle contains more than one altruist, the returned cycle is empty
            # if the cycle contains no altruists and the length of the cycle is >3, the returned cycle is empty
            cycle = altruists_to_front(cycle, altruists, max_chain_len)
            if len(cycle) == 0:
                delete = True
                break

            # in 1-cycles it does not matter if the client is altruistic or not
            if len(cycle) == 1:
                whatever_role.append(cycle[0])

            perm[cyc_num] = cycle

        if not delete:
            perm_string = str(perm)
            if perm_string not in finished_perms:
                finished_perms[perm_string] = 1
                result.append(perm)
                result_altruists.append(altruists)
                result_whatever_role.append(whatever_role)

    return result, result_altruists, result_whatever_role, finished_perms


def generate_with_chains(n, max_chain_len):
    if os.path.exists("permutations"):
        os.remove("permutations")

    s = set(np.arange(n))
    altruist_constellations = sum(map(lambda r: list(combinations(s, r)), range(1, len(s) + 1)), [])

    altruist_constellations = [list(list(y + 1 for y in x)) for x in altruist_constellations]
    altruist_constellations.insert(0, [])
    # remove altruist constellations where more than half of the clients is altruistic
    altruist_constellations_possible = []
    for x in altruist_constellations:
        if len(x) <= n / 2:
            altruist_constellations_possible.append(x)

    altruist_constellations = altruist_constellations_possible

    print(f"Generating ecgs file for n={n}. This may take some time.")

    base = np.arange(n) + 1

    final_result = []
    final_result_altruists = []
    final_result_whatever_role = []
    finished_perms_hash = dict()
    for altruists in altruist_constellations:
        x = list(itertools.permutations(base))
        result = []
        for a in x:
            result.append(to_cycles(a, n))

        result, result_altruists, result_whatever_role, finished_perms_hash = \
            filter_long_cycles_keep_chains(result, finished_perms_hash, altruists, max_chain_len)

        result = reformat(result)

        final_result += result
        final_result_altruists += result_altruists
        final_result_whatever_role += result_whatever_role

    with open(f"ecgs/ecgs_cc{max_chain_len}_{n}", 'w') as out:
        for perm, altruists, whatever_role in zip(final_result, final_result_altruists, final_result_whatever_role):
            written = 0
            for step, cycle in enumerate(perm):

                for i in cycle:
                    out.write(f"{i} ")
                    written += 1
                if step < len(perm) - 1:
                    out.write(f"0 ")
                    written += 1

            while written < 2 * n:
                out.write(f"0 ")
                written += 1

            for i in range(1, n + 1):
                if i in altruists:
                    out.write(f"1 ")
                else:
                    out.write(f"0 ")
            out.write("\n")


def main():
    n = int(sys.argv[1])
    max_chain_len = int(sys.argv[2])

    generate_with_chains(n, max_chain_len)


if __name__ == '__main__':
    main()
